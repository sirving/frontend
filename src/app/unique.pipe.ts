import { Pipe, PipeTransform } from '@angular/core';
import {isUndefined} from "util";

@Pipe({
  name: 'unique'
})
export class UniquePipe implements PipeTransform {

  transform(array: Object[], field: string):any {
    if (isUndefined(array)) {
      return array;
    }
    var newArray = [];
    for (let obj of array) {
      var isDuplicate = false;
      for (let toCompare of newArray) {
        if(obj[field] == toCompare[field]) {
          isDuplicate = true;
          break;
        }
      }
      if (!isDuplicate) {
        newArray.push(obj)
      }
    }
    return newArray;
  }

}
