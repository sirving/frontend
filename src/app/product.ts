export interface Product {
  name : string;
  interestVariant : boolean;
  rate : number;
  type : string;
}
