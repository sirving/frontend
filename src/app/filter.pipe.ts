import { Pipe, PipeTransform } from '@angular/core';
import {isUndefined} from "util";

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: Object[], field: string, value: string) {
    if (isUndefined(array) || isUndefined(value)) {
      return array;
    }
    var newArray = [];
    for (let obj of array) {
      if (obj[field] == value) {
        newArray.push(obj)
      }
    }
    return newArray;
  }

}
