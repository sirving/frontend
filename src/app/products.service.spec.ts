/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductsService } from './products.service';
import {Http, BaseRequestOptions, ResponseOptions, Response} from "@angular/http";
import {MockBackend, MockConnection} from "@angular/http/testing";
import {Product} from "./product";

describe('ProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Http, useFactory: (backend, options) => {
          return new Http(backend, options);
        },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        ProductsService
      ]
    });
  });

  it('should return a list of products',
    async(inject([ProductsService, MockBackend],
      (service: ProductsService, backend: MockBackend) => {

        backend.connections.subscribe((conn: MockConnection) => {
          const options: ResponseOptions = new ResponseOptions({body: require('../test_resources/products.json')});
          conn.mockRespond(new Response(options));
        });

        service.getProducts().subscribe(res => {
          expect(res.length).toEqual(18);
          var firstProduct : Product = res[0];
          expect(firstProduct.name).toEqual('15 days notice period');
          expect(firstProduct.type).toEqual('instant');
          expect(firstProduct.rate).toEqual(0.25582743);
        });
      })));

});
