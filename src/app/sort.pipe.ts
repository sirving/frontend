import { Pipe, PipeTransform } from '@angular/core';
import {isUndefined} from "util";

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(array: Object[], field: string, sortType: SortType) {
    if (isUndefined(array)
          || isUndefined(field)
          || isUndefined(sortType)) {
      return array;
    }
    return array.sort(function (a, b) {
      if (a[field] < b[field]) {
        return sortType == SortType.ASC ? -1 : 1;
      }
      if (a[field] > b[field]) {
        return sortType == SortType.ASC ? 1 : -1;
      }
      return 0;
    });
  }

}

export enum SortType {
  ASC,
  DESC
}
