/* tslint:disable:no-unused-variable */

import { UniquePipe } from './unique.pipe';

describe('UniquePipe', () => {

  it(`should filter to 3 items as two items share a breed`, () => {
    let pipe = new UniquePipe();

    var toFilter = [{breed:'alsatian'}, {breed:'doberman'}, {breed:'alsatian'}, {breed:'collie'}]

    var filtered = pipe.transform(toFilter, 'breed');
    expect(filtered.length).toEqual(3);
  });

  it(`should return undefined for undefined object`, () => {
    let pipe = new UniquePipe();
    expect(pipe.transform(undefined, 'breed')).toBeUndefined();
  });

});
