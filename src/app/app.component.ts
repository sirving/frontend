import { Component } from '@angular/core';
import {ProductsService} from "./products.service";
import {Product} from "./product";
import {SortType} from "./sort.pipe";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductsService]
})
export class AppComponent {

  title : String;
  products : Product[];
  limit : number;
  switchViewText : string;
  rateSortType : SortType;
  rateSortText = "Sort lowest->highest";
  selectedRate : number;

  constructor(private service: ProductsService) {
    this.service.getProducts().subscribe(
      (data: Product[]) => this.products = data
    )
    this.toggleProductView();
  }

  toggleProductView() {
    if (this.limit == 3) {
      this.title = 'All products';
      this.limit = this.products.length;
      this.switchViewText = 'View top 3 products'
    }
    else {
      this.title = 'Top 3 products';
      this.limit = 3;
      this.switchViewText = 'View all products'
    }
  }

  toggleSortRate() {
    if(this.rateSortType == SortType.ASC) {
      this.rateSortType = SortType.DESC;
      this.rateSortText = "Sort lowest->highest";
    }
    else {
      this.rateSortType = SortType.ASC;
      this.rateSortText = "Sort highest->lowest";
    }
  }

}
