import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/Rx';
import {Product} from "./product";
import { environment } from '../environments/environment';

@Injectable()
export class ProductsService {

  constructor(private http:Http) {

  }

  getProducts() {
    return this.http.get(environment.apiRoot + 'products')
      .map(res => <Product[]>res.json());
  }

}
