/* tslint:disable:no-unused-variable */

import { FilterPipe } from './filter.pipe';

describe('FilterPipe', () => {

  it(`should filter to breed 'alsatian'`, () => {
    let pipe = new FilterPipe();

    var toFilter = [{breed:'alsatian'}, {breed:'doberman'}, {breed:'alsatian'}, {breed:'collie'}]

    var filtered = pipe.transform(toFilter, 'breed', 'alsatian');
    expect(filtered.length).toEqual(2);
    for (let filteredItem of filtered) {
      expect(filteredItem.breed).toEqual('alsatian');
    }
  });

  it(`should return object array as-is if filter value is undefined`, () => {
    let pipe = new FilterPipe();

    var toFilter = [{breed:'alsatian'}, {breed:'doberman'}, {breed:'alsatian'}, {breed:'collie'}]

    var filtered = pipe.transform(toFilter, 'breed', undefined);
    expect(filtered.length).toEqual(4);
  });

  it(`should return undefined for undefined object`, () => {
    let pipe = new FilterPipe();
    expect(pipe.transform(undefined, 'breed', undefined)).toBeUndefined();
  });

});
